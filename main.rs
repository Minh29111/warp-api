// warp offers basic types via its own http library
use warp::{http, Filter};
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;
/* Arc: compiler knows when to drop a value and a read and write lock (RwLock). 
*** No two methods on different threads are writing to the same memory.
*/
use serde::{Serialize, Deserialize};
type Items = HashMap<String, i32>;

/*
Build an API
Model: An API for a grocery list. 
Add items to the list, update the quantity, delete items, and view the whole list. 
4 routes with the HTTP methods GET, DELETE, PUT, and POST.
*/
#[derive(Debug, Deserialize, Serialize, Clone)]
struct Id {
    name: String,
}
 
#[derive(Debug, Deserialize, Serialize, Clone)]
struct Item {
    name: String,
    quantity: i32,
}

#[derive(Clone)]
struct Store {
/* This type of lock allows a number of readers or at most one writer at any point in time. 
The write portion of this lock typically allows modification of the underlying data (exclusive access) 
and the read portion of this lock typically allows for read-only access (shared access). */
  grocery_list: Arc<RwLock<Items>>
}

impl Store {
    fn new() -> Self {
        Store {
            grocery_list: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}


// The method for the POST request, to add items to the list, make an HTTP POST request to a path. 
// This method has to return a proper HTTP code so the caller knows whether their call was successful
async fn update_grocery_list(
    item: Item,
    store: Store
    ) -> Result<impl warp::Reply, warp::Rejection> {
        store.grocery_list.write().insert(item.name, item.quantity);


        Ok(warp::reply::with_status(
            "Added items to the grocery list",
            http::StatusCode::CREATED,
        ))
}
async fn delete_grocery_list_item(
    id: Id,
    store: Store
    ) -> Result<impl warp::Reply, warp::Rejection> {
        store.grocery_list.write().remove(&id.name);


        Ok(warp::reply::with_status(
            "Removed item from grocery list",
            http::StatusCode::OK,
        ))
}
/* 
You’ll get a taste of async Rust when you examine the data structure behind your Arc. 
You’ll need to .read() and then .iter() over the data inside the RwLock, 
so create a new variable to return to the caller. 
Due to the nature of Rust’s ownership model, 
you can’t simply read and return the underlying list of groceries. */

async fn get_grocery_list(
    store: Store
    ) -> Result<impl warp::Reply, warp::Rejection> {
        let mut result = HashMap::new();
        let r = store.grocery_list.read();


        for (key,value) in r.iter() {
            result.insert(key, value);
        }

        Ok(warp::reply::json(
            &result
        ))
}
/* Add a new route and call the method you just created for it. 
Since you can expect a JSON for this, 
you should create a little json_body helper function to extract the Item out of the body of the HTTP request.
*/
fn delete_json() -> impl Filter<Extract = (Id,), Error = warp::Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

fn post_json() -> impl Filter<Extract = (Item,), Error = warp::Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

#[tokio::main]
async fn main() {
    let store = Store::new();
    let store_filter = warp::any().map(move || store.clone());

/* Pass the store down to each method by cloning it and creating a warp filter, 
which call in the .and() during the warp path creation. */
       let add_items = warp::post()
       .and(warp::path("v1"))
       .and(warp::path("groceries"))
       .and(warp::path::end())
       .and(post_json())
       .and(store_filter.clone())
       .and_then(update_grocery_list);

/* Now can post a list of items to our grocery list, 
but still can’t retrieve them. 
Need to create another route for the GET request. 
Main function will add this new route. 
For this new route, don’t need to parse any JSON. */

        let get_items = warp::get()
       .and(warp::path("v1"))
       .and(warp::path("groceries"))
       .and(warp::path::end())
       .and(store_filter.clone()) 
       .and_then(get_grocery_list);

       let delete_item = warp::delete()
       .and(warp::path("v1"))
       .and(warp::path("groceries"))
       .and(warp::path::end())
       .and(delete_json())
       .and(store_filter.clone())
       .and_then(delete_grocery_list_item);

       let update_item = warp::put()
       .and(warp::path("v1"))
       .and(warp::path("groceries"))
       .and(warp::path::end())
       .and(post_json())
       .and(store_filter.clone())
       .and_then(update_grocery_list);

       let routes = add_items.or(get_items).or(delete_item).or(update_item);

       warp::serve(routes)
           .run(([127, 0, 0, 1], 3030))
           .await;
}




